// append label after payment selection
jQuery('.donation__customlabel').appendTo('.wpforms-field.wpforms-field-radio');

// append total container to submit button
jQuery('.donation__total').prependTo('.wpforms-submit-container');

jQuery('.donation__total--top').prependTo('.wpforms-field-description');




// donation computation
jQuery("input[type='range']").on("input change", function() { 
	var donationAmount = parseInt(jQuery(this).val()),
		totalRaised =   parseInt(jQuery('.donation__hidden__value').val()),
		targetDonation = parseInt(jQuery(4000000)),
		estimatedRaise = totalRaised + donationAmount;

	jQuery('.donation__raised span').text(estimatedRaise);
	jQuery('.donation__total__value span').text(donationAmount);

 }); 



