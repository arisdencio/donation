<?php /* Template Name: Homepage */ ?>
<?php get_header(); ?>

<div class="donation">
	<?php the_content(); ?>

	<div class="donation__total donation__total--top">
		<div class="donation__total__value">
			<div class="donation__wrapper">
				<div>$</div>
				<span>10</span>
			</div>
		</div>
	</div>

	<div class="donation__form">
		<div class="donation__counter">
			<h2>Lorem ipsum</h2>
			<input class="donation__hidden__value" type="number" value="413078" hidden>
			<div class="donation__raised">$<span>413078</span></div>
			<span>of $4 million raised</span>
		</div>
		<?php echo do_shortcode('[wpforms id="20" title="false"]') ?>
		<div class="donation__customlabel">
			<label>Personal Info</label>
		</div>
	</div>
	<div class="donation__total">
		<div class="donation__total__value">
			<div class="donation__wrapper">
				<div>Donation total :</div>
				$<span>10</span>
			</div>
		</div>
	</div>
</div>

<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-5">
				<div class="thumb">
					<img src="<?php echo get_template_directory_uri(); ?>/img/iStock-1214595977.jpg">
				</div>
			</div>
			<div class="col-md-7">
				<div class="par-content">
					<div class="par-content-inner">
						<h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sagittis purus sit amet volutpat consequat mauris. In pellentesque massa placerat duis. Tincidunt dui ut ornare lectus sit. </h2>
						<p>Blandit aliquam etiam erat velit scelerisque in dictum non consectetur. Nisl purus in mollis nunc sed id semper risus. Bibendum enim facilisis gravida neque. Donec ultrices tincidunt arcu non sodales neque sodales ut etiam. Quis blandit turpis cursus in hac. Nunc pulvinar sapien et ligula ullamcorper malesuada proin libero nunc. Nulla facilisi cras fermentum odio. Mollis aliquam ut porttitor leo a. Quam vulputate dignissim suspendisse in est ante in nibh mauris. Mattis pellentesque id nibh tortor id aliquet lectus proin nibh.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<?php get_footer(); ?>